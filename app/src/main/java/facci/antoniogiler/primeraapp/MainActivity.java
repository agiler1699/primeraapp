package facci.antoniogiler.primeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    EditText valueFahrenheit, valueCelsius;
    TextView resultFahrenheitToCelsius, resultCelsiusToFahrenheit;
    int fahrenheit, celsius;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String lang = Locale.getDefault().getLanguage();
        Log.i("aplicacionesMoviles1",lang);

        if(lang == "en"){
            setContentView(R.layout.activity_main);
        }else{
            setContentView(R.layout.activity_main_esp);
        }
        resultFahrenheitToCelsius = (TextView) findViewById(R.id.resultFahrenheitToCelsius);
        resultCelsiusToFahrenheit = (TextView) findViewById(R.id.resultCelsiusToFahrenheit);
        valueFahrenheit  = (EditText) findViewById(R.id.valueFahrenheit);
        valueCelsius = (EditText) findViewById(R.id.valueCelsius);







        valueFahrenheit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value  = s.toString();
                if (value.length()>0){
                fahrenheit = Integer.parseInt(value);
                Double result = (fahrenheit -32) / 1.8;
                resultFahrenheitToCelsius.setText(result.toString() +"°C");}
                else{
                    resultFahrenheitToCelsius.setText("°C");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        valueCelsius.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value = s.toString();
                if (value.length()>0){
                    celsius = Integer.parseInt(value);
                    Double result = (celsius * 1.8)+32;
                    resultCelsiusToFahrenheit.setText(result.toString() +"°F");}
                else{
                    resultCelsiusToFahrenheit.setText("°F");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Log.i("aplicacionesMoviles1","Antonio Andres Giler Macias");
    }
}